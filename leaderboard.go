package gowynn

import (
	"encoding/json"
	"errors"
	"fmt"
)

type TimeFrame string

const (
	ALLTIME TimeFrame = "alltime"
	WEEKLY  TimeFrame = "weekly"
)

func (tf TimeFrame) isValid() error {
	switch tf {
	case ALLTIME, WEEKLY:
		return nil
	}

	return errors.New("Invalid TimeFrame type")
}

type Leaderboard struct {
}

// GetGuilds Returns a leaderboard of guilds with a length of 100.
// Sorted by territories currently owned, then level.
func (l Leaderboard) GetGuilds() (GuildsLeaderboard, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=statsLeaderboard&type=guild&timeframe=alltime")

	resp, err := doRequest(url)
	if err != nil {
		return GuildsLeaderboard{}, err
	}

	var result GuildsLeaderboard

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return GuildsLeaderboard{}, err
	}

	return result, nil
}

// GetPlayers Returns a leaderboard of players with a length of 100.
// Sorted by player combat level/xp.
func (l Leaderboard) GetPlayers() (PlayerLeaderboard, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=statsLeaderboard&type=player&timeframe=alltime")

	resp, err := doRequest(url)
	if err != nil {
		return PlayerLeaderboard{}, err
	}

	var result PlayerLeaderboard

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return PlayerLeaderboard{}, err
	}

	return result, nil
}

// GetPvP Returns a leaderboard of players with a length of 100.
// Sorted by PvP kills. Timeframe can be ALLTIME or WEEKLY.
func (l Leaderboard) GetPvP(timeFrame TimeFrame) (PvPLeaderboard, error) {
	if err := timeFrame.isValid(); err != nil {
		return PvPLeaderboard{}, err
	}

	url := fmt.Sprintf(apiURL+"public_api.php?action=statsLeaderboard&type=pvp&timeframe=%s", timeFrame)

	resp, err := doRequest(url)
	if err != nil {
		return PvPLeaderboard{}, err
	}

	var result PvPLeaderboard

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return PvPLeaderboard{}, err
	}

	return result, nil
}
