// gowynn is an API wrapper for wynncraft's API (https://docs.wynncraft.com)
package gowynn

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

const apiURL string = "https://api.wynncraft.com/"

type QueryType string

const (
	TYPE                QueryType = "type"
	SKILL               QueryType = "skill"
	LEVEL               QueryType = "level"
	DURABILITY          QueryType = "durability"
	HEALTH_OR_DAMAGE    QueryType = "healthOrDamage"
	DURATION            QueryType = "duration"
	BASIC_DURATION      QueryType = "basicDuration"
	NAME                QueryType = "name"
	TIER                QueryType = "tier"
	SKILLS              QueryType = "skills"
	SPRITE              QueryType = "sprite"
	IDENTIFICATIONS     QueryType = "identifications"
	ITEM_ONLY_IDS       QueryType = "itemOnlyIDs"
	CONSUMABLE_ONLY_IDS QueryType = "consumableOnlyIDs"
)

func (qt QueryType) isValid() error {
	switch qt {
	case TYPE, SKILL, LEVEL, DURABILITY, HEALTH_OR_DAMAGE, DURATION, BASIC_DURATION,
		NAME, TIER, SKILLS, SPRITE, IDENTIFICATIONS, ITEM_ONLY_IDS, CONSUMABLE_ONLY_IDS:
		return nil
	}

	return errors.New("Invalid QueryType value")
}

var client = http.Client{}

type Wynncraft struct {
	Guild       *Guild
	Player      *Player
	Leaderboard *Leaderboard
	Network     *Network
	Territory   *Territory
	Item        *Item
	Search      *Search
	Recipe      *Recipe
	Ingredient  *Ingredient
}

func NewWynncraft() *Wynncraft {
	return &Wynncraft{
		Guild:       &Guild{},
		Player:      &Player{},
		Leaderboard: &Leaderboard{},
		Network:     &Network{},
		Territory:   &Territory{},
		Item:        &Item{},
		Search:      &Search{},
		Recipe:      &Recipe{},
		Ingredient:  &Ingredient{},
	}
}

func doRequest(url string) ([]byte, error) {
	req, _ := http.NewRequestWithContext(
		context.Background(),
		http.MethodGet,
		url,
		nil)

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return body, nil
}
