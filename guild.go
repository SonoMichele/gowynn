package gowynn

import (
	"encoding/json"
	"fmt"
)

type Guild struct {
}

// GetList Returns a list of all guilds.
func (g Guild) GetList() (GuildsList, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=guildList")

	resp, err := doRequest(url)
	if err != nil {
		return GuildsList{}, err
	}

	var result GuildsList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return GuildsList{}, err
	}

	return result, nil
}

// GetStats Returns guild information, such as: level, members, territories, xp, and more.
func (g Guild) GetStats(guildName string) (GuildStats, error) {
	url := fmt.Sprintf(apiURL+"public_api.php?action=guildStats&command=%s", guildName)

	resp, err := doRequest(url)
	if err != nil {
		return GuildStats{}, err
	}

	var result GuildStats

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return GuildStats{}, err
	}

	return result, nil
}
