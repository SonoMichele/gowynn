package gowynn

import (
	"encoding/json"
	"fmt"
)

type Search struct {
}

// ByName Returns a list of guild and player names which contain the search query (case-insensitive).
func (s Search) ByName(name string) (SearchResult, error) {
	url := fmt.Sprintf(apiURL+"public_api.php?action=statsSearch&search=%s", name)

	resp, err := doRequest(url)
	if err != nil {
		return SearchResult{}, err
	}

	var result SearchResult

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return SearchResult{}, err
	}

	return result, nil
}
