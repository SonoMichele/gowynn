# Guild

## GetList() (GuildsList, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	fmt.Println(wynncraft.Guild.GetList())
}
```

## GetStats(guildName string) (GuildStats, error)
```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	guild, err := wynncraft.Guild.GetStats("Emorians")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(guild)
}
```