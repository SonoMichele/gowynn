# Search

## ByName(name string) (SearchResult, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	search, err := wynncraft.Search.ByName("SonoMichele")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(search)
}
```