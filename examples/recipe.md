# Recipe

## Get(name string) (RecipeResult, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	recipe, err := wynncraft.Recipe.Get("Boots-1-3")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(recipe)
}
```

## UpdateList() (RecipeList, error)

**Note:** This should only be used after a recipe update on Wynncraft

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	recipes, err := wynncraft.Recipe.UpdateList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(recipes)
}
```

## GetList() (RecipeList, error)

**Note:** You should use this instead of UpdateList()

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	recipes, err := wynncraft.Recipe.GetList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(recipes)
}
```

## Search(query QueryType, args string) (RecipeResult, error)

**Note:** Check [wynncraft docs](https://docs.wynncraft.com/Recipe-API/#search) for docs about this endpoint

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	recipes, err := wynncraft.Recipe.Search(gowynn.TYPE, "boots")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(recipes)
}
```
