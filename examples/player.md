# Player

## GetStats(player string) (PlayerStats, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	player, err := wynncraft.Player.GetStats("SonoMichele")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(player)
}
```

## GetUUID(username string) (PlayerUUID, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	uuid, err := wynncraft.Player.GetUUID("SonoMichele")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(uuid)
}
```