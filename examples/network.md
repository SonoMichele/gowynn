# Network

## GetServerList() (ServerList, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	servers, err := wynncraft.Network.GetServerList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(servers)
}
```

## GetPlayerSum() (PlayerSum, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	playerSum, err := wynncraft.Network.GetPlayerSum()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(playerSum)
}
```