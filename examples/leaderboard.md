# Leaderboard

## GetGuilds() (GuildsLeaderboard, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	guilds, err := wynncraft.Leaderboard.GetGuilds()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(guilds)
}
```

## GetPlayers() (PlayerLeaderboard, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	players, err := wynncraft.Leaderboard.GetPlayers()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(players)
}
```

## GetPvP(timeFrame TimeFrame) (PvPLeaderboard, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	pvp, err := wynncraft.Leaderboard.GetPvP(gowynn.ALLTIME)  // or gowynn.WEEKLY
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(pvp)
}
```