# Ingredient

## Get(name string) (IngredientResult, error)

**Note:** This function automatically replaces spaces with _ so you don't have to do it 
(check [wynncraft docs](https://docs.wynncraft.com/Ingredient-API/#get))

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	ingredient, err := wynncraft.Ingredient.Get("Antique Metal")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(ingredient)
}
```

## UpdateList() (IngredientList, error)

**Note:** This should only be used after an ingredient update on Wynncraft

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	ingredients, err := wynncraft.Ingredient.UpdateList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(ingredients)
}
```

## GetList() (IngredientList, error)

**Note:** You should use this instead of UpdateList()

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	ingredients, err := wynncraft.Ingredient.UpdateList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(ingredients)
}
```

## Search(query QueryType, args string) (IngredientResult, error)

**Note:** Check [wynncraft docs](https://docs.wynncraft.com/Ingredient-API/#search) for docs about this endpoint

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	ingredients, err := wynncraft.Ingredient.Search(gowynn.TIER, "0")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(ingredients)
}
```