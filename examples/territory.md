# Territory

## GetList() (TerritoryList, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	territory, err := wynncraft.Territory.GetList()
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(territory)
}
```