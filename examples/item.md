# Item

## SearchByCategory(category ItemCategory) (ItemsList, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	items, err := wynncraft.Item.SearchByCategory(gowynn.WAND)
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(items)
}
```

## SearchByName(name string) (ItemsList, error)

```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()

	items, err := wynncraft.Item.SearchByName("e")
	if err != nil {
		fmt.Println(err)

		return
	}
	
	fmt.Println(items)
}
```