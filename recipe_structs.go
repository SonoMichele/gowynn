package gowynn

type RecipeResult struct {
	Kind      string `json:"kind"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Version   string `json:"version"`
	Data      []struct {
		Level struct {
			Minimum int `json:"minimum"`
			Maximum int `json:"maximum"`
		} `json:"level"`
		Type      string `json:"type"`
		Skill     string `json:"skill"`
		Materials []struct {
			Item   string `json:"item"`
			Amount int    `json:"amount"`
		} `json:"materials"`
		HealthOrDamage struct {
			Minimum int `json:"minimum"`
			Maximum int `json:"maximum"`
		} `json:"healthOrDamage"`
		Durability struct {
			Minimum int `json:"minimum"`
			Maximum int `json:"maximum"`
		} `json:"durability"`
		ID string `json:"id"`
	} `json:"data"`
}

type RecipeList struct {
	Kind      string   `json:"kind"`
	Code      int      `json:"code"`
	Timestamp int64    `json:"timestamp"`
	Version   string   `json:"version"`
	Data      []string `json:"data"`
}
