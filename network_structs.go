package gowynn

type ServerList struct {
	Request struct {
		Timestamp int     `json:"timestamp"`
		Version   float64 `json:"version"`
	} `json:"request"`
	TEST []string `json:"TEST"`
	WC1  []string `json:"WC1"`
	WC10 []string `json:"WC10"`
	WC11 []string `json:"WC11"`
	WC12 []string `json:"WC12"`
	WC13 []string `json:"WC13"`
	WC14 []string `json:"WC14"`
	WC15 []string `json:"WC15"`
	WC16 []string `json:"WC16"`
	WC17 []string `json:"WC17"`
	WC18 []string `json:"WC18"`
	WC19 []string `json:"WC19"`
	WC2  []string `json:"WC2"`
	WC20 []string `json:"WC20"`
	WC21 []string `json:"WC21"`
	WC22 []string `json:"WC22"`
	WC23 []string `json:"WC23"`
	WC24 []string `json:"WC24"`
	WC25 []string `json:"WC25"`
	WC26 []string `json:"WC26"`
	WC27 []string `json:"WC27"`
	WC28 []string `json:"WC28"`
	WC29 []string `json:"WC29"`
	WC3  []string `json:"WC3"`
	WC30 []string `json:"WC30"`
	WC31 []string `json:"WC31"`
	WC32 []string `json:"WC32"`
	WC33 []string `json:"WC33"`
	WC34 []string `json:"WC34"`
	WC35 []string `json:"WC35"`
	WC36 []string `json:"WC36"`
	WC37 []string `json:"WC37"`
	WC38 []string `json:"WC38"`
	WC39 []string `json:"WC39"`
	WC4  []string `json:"WC4"`
	WC40 []string `json:"WC40"`
	WC41 []string `json:"WC41"`
	WC42 []string `json:"WC42"`
	WC43 []string `json:"WC43"`
	WC44 []string `json:"WC44"`
	WC45 []string `json:"WC45"`
	WC46 []string `json:"WC46"`
	WC47 []string `json:"WC47"`
	WC48 []string `json:"WC48"`
	WC49 []string `json:"WC49"`
	WC5  []string `json:"WC5"`
	WC50 []string `json:"WC50"`
	WC51 []string `json:"WC51"`
	WC52 []string `json:"WC52"`
	WC53 []string `json:"WC53"`
	WC54 []string `json:"WC54"`
	WC55 []string `json:"WC55"`
	WC56 []string `json:"WC56"`
	WC57 []string `json:"WC57"`
	WC6  []string `json:"WC6"`
	WC7  []string `json:"WC7"`
	WC8  []string `json:"WC8"`
	WC9  []string `json:"WC9"`
}

type PlayerSum struct {
	PlayersOnline int `json:"players_online"`
	Request       struct {
		Timestamp int     `json:"timestamp"`
		Version   float64 `json:"version"`
	} `json:"request"`
}
