package gowynn

type GuildsList struct {
	Guilds  []string `json:"guilds"`
	Request struct {
		Timestamp int64 `json:"timestamp"`
		Version   int64 `json:"version"`
	} `json:"request"`
}

type GuildStats struct {
	Banner struct {
		Base   string `json:"base"`
		Layers []struct {
			Colour  string `json:"colour"`
			Pattern string `json:"pattern"`
		} `json:"layers"`
		Tier int64 `json:"tier"`
	} `json:"banner"`
	Created         string `json:"created"`
	CreatedFriendly string `json:"createdFriendly"`
	Level           int64  `json:"level"`
	Members         []struct {
		Contributed    int64  `json:"contributed"`
		Joined         string `json:"joined"`
		JoinedFriendly string `json:"joinedFriendly"`
		Name           string `json:"name"`
		Rank           string `json:"rank"`
		UUID           string `json:"uuid"`
	} `json:"members"`
	Name        string  `json:"name"`
	Prefix      string  `json:"prefix"`
	Territories int64   `json:"territories"`
	Xp          float64 `json:"xp"`
	Request     struct {
		Timestamp int64 `json:"timestamp"`
		Version   int64 `json:"version"`
	} `json:"request"`
}
