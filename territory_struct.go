package gowynn

type TerritoryList struct {
	Territories Territories `json:"territories"`
	Request     Request     `json:"request"`
}
type Location struct {
	StartX int `json:"startX"`
	StartY int `json:"startY"`
	EndX   int `json:"endX"`
	EndY   int `json:"endY"`
}
type Ragni struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EmeraldTrail struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RagniNorthEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RagniNorthSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RagniPlains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MalticCoast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MalticPlains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PigmenRavinesEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SouthPigmenRavines struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TimeValley struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SanctuaryBridge struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type ElkurnFields struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NivlaWoods struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type SouthNivlaWoods struct {
	Territory string `json:"territory"`
	Guild     string `json:"guild"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Elkurn struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorruptedRoad struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasFarSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasCloseSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SouthFarmersValley struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type ArachnidRoute struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type TowerOfAscension struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type MageIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TwainMansion struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsSouthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsNorthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsUpperNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakBridgeTransition struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GreatBridgeNesaak struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JungleLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JungleUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TempleOfLegends struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekEastUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekWestMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertEastUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertMidUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MummySTomb struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SavannahEastUpper struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SavannahWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LionLair struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PlainsCoast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractPlainsWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AncientNemract struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CathedralHarbour struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RoosterIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Selchar struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DurumIslesUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DurumIslesLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SkiensIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NodgujNation struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DeadIslandSouthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DeadIslandSouthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type VolcanoUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TreeIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TernavesPlainsUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MiningBaseUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakTransition struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NetherPlainsLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MineBasePlains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NetherPlainsUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasTrailWestPlains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarGateEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarFarmPlainsEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Hive struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarPlainsEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarPlainsWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampEastMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampWestMidUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampDarkForestTransitionLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampDarkForestTransitionUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EntranceToOlux struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampMountainBase struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampMountainTransitionLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampMountainTransitionMidUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type QuartzMinesSouthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type QuartzMinesNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SunsparkCamp struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type OrcRoad struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SablestoneCamp struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type IronRoad struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LlevigarFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GoblinPlainsEast struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LeadinFortress struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EfilimVillage struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type EfilimEastPlains struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LightForestNorthEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestSouthEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestSouthExit struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestEastMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type HobbitRiver struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestCanyon struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LoneFarmstead struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GelibordCorruptedFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TaprootDescent struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FortressSouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TwistedHousing struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type VisceraPitsWest struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AbandonedManor struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type KanderMines struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type VisceraPitsEast struct {
	Territory string `json:"territory"`
	Guild     string `json:"guild"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type OldCrossroadsSouth struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Lexdale struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DecayedBasin struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CinfrasEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FallenVillage struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type GuildHall struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CinfrasSSmallFarm struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CinfrasCountyMidLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CinfrasCountyUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GyliaLakeSouthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GyliaLakeNorthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JitakSFarm struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AldoreiValleyMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AldoreiSRiver struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AldoreiSNorthExit struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type PathToTheArch struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BurningFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CinfrasThanosTransition struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToThanos struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MilitaryBase struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MilitaryBaseLower struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToOzothSSpireMid struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type BanditCaveLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonEntranceWaterfall struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonPathSouthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonUpperNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonPathSouthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BanditCampExit struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ThanosValleyWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonWalkWay struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonMountainSouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonFortress struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonDropoff struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BanditsToll struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MountainPath struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffSideOfTheLost struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TempleOfTheLostEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type HiveSouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffsideWaterfall struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AirTempleLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffsideLake struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type KandonBeda struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffsidePassage struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EntranceToTheseadNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ChainedHouse struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RanolSFarm struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Thesead struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Eltom struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LavaLake struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CraterDescent struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type VolcanicSlope struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TempleIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DernelJungleLower struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type DernelJungleUpper struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CorkusCastle struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FallenFactory struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusCityMine struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FactoryEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusForestNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AvosWorkshop struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CorkusCountryside struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RuinedHouses struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AvosTemple struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusOutskirts struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SkyCastle struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToAhmsordUpper struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type OldCoalMine struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AstraulusTower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AhmsordOutskirts struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AngelRefuge struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CentralIslands struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SkyFalls struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RaiderSBaseLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JofashDocks struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Lusuco struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PhinasFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CinfrasOutskirts struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Llevigar struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type HerbCave struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type IcyIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FlerisTrail struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AbandonedPass struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SouthernOutpost struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusSeaCove struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TheBrokenRoad struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GreyRuins struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type ForestOfEyes struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Lutho struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ToxicDrip struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GatewayToNothing struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FinalStep struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type TheGate struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LuminousPlateau struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type PrimalFen struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type OtherwordlyMonolith struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type NexusOfLight struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type RagniMainEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type KatoaRanch struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CoastalTrail struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Plains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LittleWood struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RoadToTimeValley struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NivlaWoodsExit struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type RoadToElkurn struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NorthFarmersValley struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type HalfMoonIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BobSTomb struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsLowerNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsMidNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JungleMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CityOfTroms struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertEastMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertMidLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SavannahWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractTown struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractPlainsEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TheBearZoo struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DurumIslesCenter struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PirateTown struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DeadIslandNorthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MaroPeaks struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Ternaves struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MiningBaseLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PlainsLake struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasTrailEastPlains struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarGateWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Cinfras struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarPlainsEastUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampWestMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampEastUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampPlainsBasin struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampMountainTransitionUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type QuartzMinesNorthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MeteorCrater struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GoblinPlainsWest struct {
	Territory string `json:"territory"`
	Guild     string `json:"guild"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type PreLightForestTransition struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EfilimSouthPlains struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LightForestEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestNorthExit struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestEastUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MantisNest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Gelibord struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FortressNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LexdalesPrison struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MesquisTower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DarkForestVillage struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FungalGrove struct {
	Territory string `json:"territory"`
	Guild     string `json:"guild"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type MushroomHill struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AldoreiValleySouthEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CinfrasCountyMidUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GyliaLakeNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AldoreiValleyLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AldoreiSWaterfall struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type GhostlyPath struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Thanos struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MilitaryBaseUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToOzothSSpireUpper struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CanyonPathNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonLowerSouthEast struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonValleySouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonMountainEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonSurvivor struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type WizardTowerNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ValleyOfTheLost struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonHighPath struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AirTempleUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type KandonFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EntranceToTheseadSouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TheseadSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MoltenHeightsPortal struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ActiveVolcano struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SnailIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusCity struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RoadToMine struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusForestSouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusDocks struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusStatue struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type FrozenFort struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type KandonRidge struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MoltenReach struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type WybelIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RaiderSBaseUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SantaSHideout struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AldoreiLowlands struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RegularIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RoyalGate struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LostAtoll struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TheSilentRoad struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ForgottenTown struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathsOfSludge struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type VoidValley struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type HeavenlyIngress struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AzureFrontier struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type NivlaWoodsEntrance struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Maltic struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AbandonedFarm struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NorthNivlaWoods struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Detlas struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TwainLake struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakVillage struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JungleLake struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekWestUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AlmujCity struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Bremminglar struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractCathedral struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DurumIslesEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DeadIslandNorthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TernavesPlainsLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DetlasSavannahTransition struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LlevigarFarmPlainsWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampDarkForestTransitionMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampMountainTransitionMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type OrcLake struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ForgottenPath struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type EfilimSouthEastPlains struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LightForestWestMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToCinfras struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MansionOfInsanity struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToTalor struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type HeartOfDecay struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type CinfrasCountyLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GertCamp struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type AldoreiSArch struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type PathToMilitaryBase struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BanditCaveUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonPathNorthMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonWaterfallMidNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type MountainEdge struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffsideValley struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CliffsidePassageNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EntranceToRodoroc struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Ahmsord struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusCitySouth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusMountain struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BloodyBeach struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DragonlingNests struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SkyIslandAscent struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type IcyDescent struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type TwistedRidge struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LighthousePlateau struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SinisterForest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BizarrePassage struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToLight struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type RagniEastSuburbs struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractQuarry struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type NivlaWoodsEdge struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type GreatBridgeJungle struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesertUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NemractRoad struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DujgonNation struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DesolateValley struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LlevigarPlainsWestLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Olux struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LoamsproutCamp struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AldoreiValleyWestEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type GelibordCastle struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type OldCrossroadsNorth struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type GyliaLakeSouthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type BurningAirship struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonWaterfallNorth struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ThanosExitUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CanyonOfTheLost struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LavaLakeBridge struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LegendaryIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToAhmsordLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type JofashTunnel struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type OrcBattlegrounds struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ToxicCaves struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PigmenRavines struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type NesaakPlainsSouthWest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SavannahEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type VolcanoLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type SwampEastMidUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RoadToLightForest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type EntranceToKander struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type AldoreiValleyUpper struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ThanosExit struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CherryBlossomForest struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type CorkusSeaPort struct {
	Territory string   `json:"territory"`
	Guild     string   `json:"guild"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type Rodoroc struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type FieldOfLife struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type NetherGate struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type ZhightIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type QuartzMinesSouthEast struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DarkForestCinfrasTransition struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type KroltonSCave struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type SwampIsland struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type RymekEastMid struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type LightForestEastLower struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type DernelJungleMid struct {
	Territory string `json:"territory"`
	Guild     string `json:"guild"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type LlevigarEntrance struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type WormTunnel struct {
	Territory string   `json:"territory"`
	Acquired  string   `json:"acquired"`
	Attacker  string   `json:"attacker"`
	Location  Location `json:"location"`
}
type PathToOzothSSpireLower struct {
	Territory string `json:"territory"`
	Acquired  string `json:"acquired"`
	Attacker  string `json:"attacker"`
}
type Territories struct {
	Ragni                           Ragni                           `json:"Ragni"`
	EmeraldTrail                    EmeraldTrail                    `json:"Emerald Trail"`
	RagniNorthEntrance              RagniNorthEntrance              `json:"Ragni North Entrance"`
	RagniNorthSuburbs               RagniNorthSuburbs               `json:"Ragni North Suburbs"`
	RagniPlains                     RagniPlains                     `json:"Ragni Plains"`
	MalticCoast                     MalticCoast                     `json:"Maltic Coast"`
	MalticPlains                    MalticPlains                    `json:"Maltic Plains"`
	PigmenRavinesEntrance           PigmenRavinesEntrance           `json:"Pigmen Ravines Entrance"`
	SouthPigmenRavines              SouthPigmenRavines              `json:"South Pigmen Ravines"`
	TimeValley                      TimeValley                      `json:"Time Valley"`
	SanctuaryBridge                 SanctuaryBridge                 `json:"Sanctuary Bridge"`
	ElkurnFields                    ElkurnFields                    `json:"Elkurn Fields"`
	NivlaWoods                      NivlaWoods                      `json:"Nivla Woods"`
	SouthNivlaWoods                 SouthNivlaWoods                 `json:"South Nivla Woods"`
	Elkurn                          Elkurn                          `json:"Elkurn"`
	CorruptedRoad                   CorruptedRoad                   `json:"Corrupted Road"`
	DetlasFarSuburbs                DetlasFarSuburbs                `json:"Detlas Far Suburbs"`
	DetlasCloseSuburbs              DetlasCloseSuburbs              `json:"Detlas Close Suburbs"`
	SouthFarmersValley              SouthFarmersValley              `json:"South Farmers Valley"`
	ArachnidRoute                   ArachnidRoute                   `json:"Arachnid Route"`
	TowerOfAscension                TowerOfAscension                `json:"Tower of Ascension"`
	MageIsland                      MageIsland                      `json:"Mage Island"`
	TwainMansion                    TwainMansion                    `json:"Twain Mansion"`
	NesaakPlainsSouthEast           NesaakPlainsSouthEast           `json:"Nesaak Plains South East"`
	NesaakPlainsNorthEast           NesaakPlainsNorthEast           `json:"Nesaak Plains North East"`
	NesaakPlainsUpperNorthWest      NesaakPlainsUpperNorthWest      `json:"Nesaak Plains Upper North West"`
	NesaakBridgeTransition          NesaakBridgeTransition          `json:"Nesaak Bridge Transition"`
	GreatBridgeNesaak               GreatBridgeNesaak               `json:"Great Bridge Nesaak"`
	JungleLower                     JungleLower                     `json:"Jungle Lower"`
	JungleUpper                     JungleUpper                     `json:"Jungle Upper"`
	TempleOfLegends                 TempleOfLegends                 `json:"Temple of Legends"`
	RymekEastLower                  RymekEastLower                  `json:"Rymek East Lower"`
	RymekEastUpper                  RymekEastUpper                  `json:"Rymek East Upper"`
	RymekWestMid                    RymekWestMid                    `json:"Rymek West Mid"`
	DesertEastUpper                 DesertEastUpper                 `json:"Desert East Upper"`
	DesertEastLower                 DesertEastLower                 `json:"Desert East Lower"`
	DesertMidUpper                  DesertMidUpper                  `json:"Desert Mid-Upper"`
	DesertLower                     DesertLower                     `json:"Desert Lower"`
	MummySTomb                      MummySTomb                      `json:"Mummy's Tomb"`
	DesertWestLower                 DesertWestLower                 `json:"Desert West Lower"`
	SavannahEastUpper               SavannahEastUpper               `json:"Savannah East Upper"`
	SavannahWestUpper               SavannahWestUpper               `json:"Savannah West Upper"`
	LionLair                        LionLair                        `json:"Lion Lair"`
	PlainsCoast                     PlainsCoast                     `json:"Plains Coast"`
	NemractPlainsWest               NemractPlainsWest               `json:"Nemract Plains West"`
	AncientNemract                  AncientNemract                  `json:"Ancient Nemract"`
	CathedralHarbour                CathedralHarbour                `json:"Cathedral Harbour"`
	RoosterIsland                   RoosterIsland                   `json:"Rooster Island"`
	Selchar                         Selchar                         `json:"Selchar"`
	DurumIslesUpper                 DurumIslesUpper                 `json:"Durum Isles Upper"`
	DurumIslesLower                 DurumIslesLower                 `json:"Durum Isles Lower"`
	SkiensIsland                    SkiensIsland                    `json:"Skiens Island"`
	NodgujNation                    NodgujNation                    `json:"Nodguj Nation"`
	DeadIslandSouthEast             DeadIslandSouthEast             `json:"Dead Island South East"`
	DeadIslandSouthWest             DeadIslandSouthWest             `json:"Dead Island South West"`
	VolcanoUpper                    VolcanoUpper                    `json:"Volcano Upper"`
	TreeIsland                      TreeIsland                      `json:"Tree Island"`
	TernavesPlainsUpper             TernavesPlainsUpper             `json:"Ternaves Plains Upper"`
	MiningBaseUpper                 MiningBaseUpper                 `json:"Mining Base Upper"`
	NesaakTransition                NesaakTransition                `json:"Nesaak Transition"`
	NetherPlainsLower               NetherPlainsLower               `json:"Nether Plains Lower"`
	MineBasePlains                  MineBasePlains                  `json:"Mine Base Plains"`
	NetherPlainsUpper               NetherPlainsUpper               `json:"Nether Plains Upper"`
	DetlasTrailWestPlains           DetlasTrailWestPlains           `json:"Detlas Trail West Plains"`
	LlevigarGateEast                LlevigarGateEast                `json:"Llevigar Gate East"`
	LlevigarFarmPlainsEast          LlevigarFarmPlainsEast          `json:"Llevigar Farm Plains East"`
	Hive                            Hive                            `json:"Hive"`
	LlevigarPlainsEastLower         LlevigarPlainsEastLower         `json:"Llevigar Plains East Lower"`
	LlevigarPlainsWestUpper         LlevigarPlainsWestUpper         `json:"Llevigar Plains West Upper"`
	SwampWestLower                  SwampWestLower                  `json:"Swamp West Lower"`
	SwampEastMid                    SwampEastMid                    `json:"Swamp East Mid"`
	SwampWestMidUpper               SwampWestMidUpper               `json:"Swamp West Mid-Upper"`
	SwampWestUpper                  SwampWestUpper                  `json:"Swamp West Upper"`
	SwampDarkForestTransitionLower  SwampDarkForestTransitionLower  `json:"Swamp Dark Forest Transition Lower"`
	SwampDarkForestTransitionUpper  SwampDarkForestTransitionUpper  `json:"Swamp Dark Forest Transition Upper"`
	EntranceToOlux                  EntranceToOlux                  `json:"Entrance to Olux"`
	SwampMountainBase               SwampMountainBase               `json:"Swamp Mountain Base"`
	SwampMountainTransitionLower    SwampMountainTransitionLower    `json:"Swamp Mountain Transition Lower"`
	SwampMountainTransitionMidUpper SwampMountainTransitionMidUpper `json:"Swamp Mountain Transition Mid-Upper"`
	QuartzMinesSouthWest            QuartzMinesSouthWest            `json:"Quartz Mines South West"`
	QuartzMinesNorthWest            QuartzMinesNorthWest            `json:"Quartz Mines North West"`
	SunsparkCamp                    SunsparkCamp                    `json:"Sunspark Camp"`
	OrcRoad                         OrcRoad                         `json:"Orc Road"`
	SablestoneCamp                  SablestoneCamp                  `json:"Sablestone Camp"`
	IronRoad                        IronRoad                        `json:"Iron Road"`
	LlevigarFarm                    LlevigarFarm                    `json:"Llevigar Farm"`
	GoblinPlainsEast                GoblinPlainsEast                `json:"Goblin Plains East"`
	LeadinFortress                  LeadinFortress                  `json:"Leadin Fortress"`
	EfilimVillage                   EfilimVillage                   `json:"Efilim Village"`
	EfilimEastPlains                EfilimEastPlains                `json:"Efilim East Plains"`
	LightForestNorthEntrance        LightForestNorthEntrance        `json:"Light Forest North Entrance"`
	LightForestSouthEntrance        LightForestSouthEntrance        `json:"Light Forest South Entrance"`
	LightForestSouthExit            LightForestSouthExit            `json:"Light Forest South Exit"`
	LightForestWestLower            LightForestWestLower            `json:"Light Forest West Lower"`
	LightForestWestUpper            LightForestWestUpper            `json:"Light Forest West Upper"`
	LightForestEastMid              LightForestEastMid              `json:"Light Forest East Mid"`
	HobbitRiver                     HobbitRiver                     `json:"Hobbit River"`
	LightForestCanyon               LightForestCanyon               `json:"Light Forest Canyon"`
	LoneFarmstead                   LoneFarmstead                   `json:"Lone Farmstead"`
	GelibordCorruptedFarm           GelibordCorruptedFarm           `json:"Gelibord Corrupted Farm"`
	TaprootDescent                  TaprootDescent                  `json:"Taproot Descent"`
	FortressSouth                   FortressSouth                   `json:"Fortress South"`
	TwistedHousing                  TwistedHousing                  `json:"Twisted Housing"`
	VisceraPitsWest                 VisceraPitsWest                 `json:"Viscera Pits West"`
	AbandonedManor                  AbandonedManor                  `json:"Abandoned Manor"`
	KanderMines                     KanderMines                     `json:"Kander Mines"`
	VisceraPitsEast                 VisceraPitsEast                 `json:"Viscera Pits East"`
	OldCrossroadsSouth              OldCrossroadsSouth              `json:"Old Crossroads South"`
	Lexdale                         Lexdale                         `json:"Lexdale"`
	DecayedBasin                    DecayedBasin                    `json:"Decayed Basin"`
	CinfrasEntrance                 CinfrasEntrance                 `json:"Cinfras Entrance"`
	FallenVillage                   FallenVillage                   `json:"Fallen Village"`
	GuildHall                       GuildHall                       `json:"Guild Hall"`
	CinfrasSSmallFarm               CinfrasSSmallFarm               `json:"Cinfras's Small Farm"`
	CinfrasCountyMidLower           CinfrasCountyMidLower           `json:"Cinfras County Mid-Lower"`
	CinfrasCountyUpper              CinfrasCountyUpper              `json:"Cinfras County Upper"`
	GyliaLakeSouthWest              GyliaLakeSouthWest              `json:"Gylia Lake South West"`
	GyliaLakeNorthEast              GyliaLakeNorthEast              `json:"Gylia Lake North East"`
	JitakSFarm                      JitakSFarm                      `json:"Jitak's Farm"`
	AldoreiValleyMid                AldoreiValleyMid                `json:"Aldorei Valley Mid"`
	AldoreiSRiver                   AldoreiSRiver                   `json:"Aldorei's River"`
	AldoreiSNorthExit               AldoreiSNorthExit               `json:"Aldorei's North Exit"`
	PathToTheArch                   PathToTheArch                   `json:"Path To The Arch"`
	BurningFarm                     BurningFarm                     `json:"Burning Farm"`
	CinfrasThanosTransition         CinfrasThanosTransition         `json:"Cinfras Thanos Transition"`
	PathToThanos                    PathToThanos                    `json:"Path To Thanos"`
	MilitaryBase                    MilitaryBase                    `json:"Military Base"`
	MilitaryBaseLower               MilitaryBaseLower               `json:"Military Base Lower"`
	PathToOzothSSpireMid            PathToOzothSSpireMid            `json:"Path To Ozoth's Spire Mid"`
	BanditCaveLower                 BanditCaveLower                 `json:"Bandit Cave Lower"`
	CanyonEntranceWaterfall         CanyonEntranceWaterfall         `json:"Canyon Entrance Waterfall"`
	CanyonPathSouthEast             CanyonPathSouthEast             `json:"Canyon Path South East"`
	CanyonUpperNorthWest            CanyonUpperNorthWest            `json:"Canyon Upper North West"`
	CanyonPathSouthWest             CanyonPathSouthWest             `json:"Canyon Path South West"`
	BanditCampExit                  BanditCampExit                  `json:"Bandit Camp Exit"`
	ThanosValleyWest                ThanosValleyWest                `json:"Thanos Valley West"`
	CanyonWalkWay                   CanyonWalkWay                   `json:"Canyon Walk Way"`
	CanyonMountainSouth             CanyonMountainSouth             `json:"Canyon Mountain South"`
	CanyonFortress                  CanyonFortress                  `json:"Canyon Fortress"`
	CanyonDropoff                   CanyonDropoff                   `json:"Canyon Dropoff"`
	BanditsToll                     BanditsToll                     `json:"Bandits Toll"`
	MountainPath                    MountainPath                    `json:"Mountain Path"`
	CliffSideOfTheLost              CliffSideOfTheLost              `json:"Cliff Side of the Lost"`
	TempleOfTheLostEast             TempleOfTheLostEast             `json:"Temple of the Lost East"`
	HiveSouth                       HiveSouth                       `json:"Hive South"`
	CliffsideWaterfall              CliffsideWaterfall              `json:"Cliffside Waterfall"`
	AirTempleLower                  AirTempleLower                  `json:"Air Temple Lower"`
	CliffsideLake                   CliffsideLake                   `json:"Cliffside Lake"`
	KandonBeda                      KandonBeda                      `json:"Kandon-Beda"`
	CliffsidePassage                CliffsidePassage                `json:"Cliffside Passage"`
	EntranceToTheseadNorth          EntranceToTheseadNorth          `json:"Entrance to Thesead North"`
	ChainedHouse                    ChainedHouse                    `json:"Chained House"`
	RanolSFarm                      RanolSFarm                      `json:"Ranol's Farm"`
	Thesead                         Thesead                         `json:"Thesead"`
	Eltom                           Eltom                           `json:"Eltom"`
	LavaLake                        LavaLake                        `json:"Lava Lake"`
	CraterDescent                   CraterDescent                   `json:"Crater Descent"`
	VolcanicSlope                   VolcanicSlope                   `json:"Volcanic Slope"`
	TempleIsland                    TempleIsland                    `json:"Temple Island"`
	DernelJungleLower               DernelJungleLower               `json:"Dernel Jungle Lower"`
	DernelJungleUpper               DernelJungleUpper               `json:"Dernel Jungle Upper"`
	CorkusCastle                    CorkusCastle                    `json:"Corkus Castle"`
	FallenFactory                   FallenFactory                   `json:"Fallen Factory"`
	CorkusCityMine                  CorkusCityMine                  `json:"Corkus City Mine"`
	FactoryEntrance                 FactoryEntrance                 `json:"Factory Entrance"`
	CorkusForestNorth               CorkusForestNorth               `json:"Corkus Forest North"`
	AvosWorkshop                    AvosWorkshop                    `json:"Avos Workshop"`
	CorkusCountryside               CorkusCountryside               `json:"Corkus Countryside"`
	RuinedHouses                    RuinedHouses                    `json:"Ruined Houses"`
	AvosTemple                      AvosTemple                      `json:"Avos Temple"`
	CorkusOutskirts                 CorkusOutskirts                 `json:"Corkus Outskirts"`
	SkyCastle                       SkyCastle                       `json:"Sky Castle"`
	PathToAhmsordUpper              PathToAhmsordUpper              `json:"Path to Ahmsord Upper"`
	OldCoalMine                     OldCoalMine                     `json:"Old Coal Mine"`
	AstraulusTower                  AstraulusTower                  `json:"Astraulus' Tower"`
	AhmsordOutskirts                AhmsordOutskirts                `json:"Ahmsord Outskirts"`
	AngelRefuge                     AngelRefuge                     `json:"Angel Refuge"`
	CentralIslands                  CentralIslands                  `json:"Central Islands"`
	SkyFalls                        SkyFalls                        `json:"Sky Falls"`
	RaiderSBaseLower                RaiderSBaseLower                `json:"Raider's Base Lower"`
	JofashDocks                     JofashDocks                     `json:"Jofash Docks"`
	Lusuco                          Lusuco                          `json:"Lusuco"`
	PhinasFarm                      PhinasFarm                      `json:"Phinas Farm"`
	CinfrasOutskirts                CinfrasOutskirts                `json:"Cinfras Outskirts"`
	Llevigar                        Llevigar                        `json:"Llevigar"`
	HerbCave                        HerbCave                        `json:"Herb Cave"`
	IcyIsland                       IcyIsland                       `json:"Icy Island"`
	FlerisTrail                     FlerisTrail                     `json:"Fleris Trail"`
	AbandonedPass                   AbandonedPass                   `json:"Abandoned Pass"`
	SouthernOutpost                 SouthernOutpost                 `json:"Southern Outpost"`
	CorkusSeaCove                   CorkusSeaCove                   `json:"Corkus Sea Cove"`
	TheBrokenRoad                   TheBrokenRoad                   `json:"The Broken Road"`
	GreyRuins                       GreyRuins                       `json:"Grey Ruins"`
	ForestOfEyes                    ForestOfEyes                    `json:"Forest of Eyes"`
	Lutho                           Lutho                           `json:"Lutho"`
	ToxicDrip                       ToxicDrip                       `json:"Toxic Drip"`
	GatewayToNothing                GatewayToNothing                `json:"Gateway to Nothing"`
	FinalStep                       FinalStep                       `json:"Final Step"`
	TheGate                         TheGate                         `json:"The Gate"`
	LuminousPlateau                 LuminousPlateau                 `json:"Luminous Plateau"`
	PrimalFen                       PrimalFen                       `json:"Primal Fen"`
	OtherwordlyMonolith             OtherwordlyMonolith             `json:"Otherwordly Monolith"`
	NexusOfLight                    NexusOfLight                    `json:"Nexus of Light"`
	RagniMainEntrance               RagniMainEntrance               `json:"Ragni Main Entrance"`
	KatoaRanch                      KatoaRanch                      `json:"Katoa Ranch"`
	CoastalTrail                    CoastalTrail                    `json:"Coastal Trail"`
	Plains                          Plains                          `json:"Plains"`
	LittleWood                      LittleWood                      `json:"Little Wood"`
	RoadToTimeValley                RoadToTimeValley                `json:"Road to Time Valley"`
	NivlaWoodsExit                  NivlaWoodsExit                  `json:"Nivla Woods Exit"`
	RoadToElkurn                    RoadToElkurn                    `json:"Road to Elkurn"`
	DetlasSuburbs                   DetlasSuburbs                   `json:"Detlas Suburbs"`
	NorthFarmersValley              NorthFarmersValley              `json:"North Farmers Valley"`
	HalfMoonIsland                  HalfMoonIsland                  `json:"Half Moon Island"`
	BobSTomb                        BobSTomb                        `json:"Bob's Tomb"`
	NesaakPlainsLowerNorthWest      NesaakPlainsLowerNorthWest      `json:"Nesaak Plains Lower North West"`
	NesaakPlainsMidNorthWest        NesaakPlainsMidNorthWest        `json:"Nesaak Plains Mid North West"`
	JungleMid                       JungleMid                       `json:"Jungle Mid"`
	CityOfTroms                     CityOfTroms                     `json:"City of Troms"`
	RymekWestLower                  RymekWestLower                  `json:"Rymek West Lower"`
	DesertEastMid                   DesertEastMid                   `json:"Desert East Mid"`
	DesertMidLower                  DesertMidLower                  `json:"Desert Mid-Lower"`
	DesertWestUpper                 DesertWestUpper                 `json:"Desert West Upper"`
	SavannahWestLower               SavannahWestLower               `json:"Savannah West Lower"`
	NemractTown                     NemractTown                     `json:"Nemract Town"`
	NemractPlainsEast               NemractPlainsEast               `json:"Nemract Plains East"`
	TheBearZoo                      TheBearZoo                      `json:"The Bear Zoo"`
	DurumIslesCenter                DurumIslesCenter                `json:"Durum Isles Center"`
	PirateTown                      PirateTown                      `json:"Pirate Town"`
	DeadIslandNorthEast             DeadIslandNorthEast             `json:"Dead Island North East"`
	MaroPeaks                       MaroPeaks                       `json:"Maro Peaks"`
	Ternaves                        Ternaves                        `json:"Ternaves"`
	MiningBaseLower                 MiningBaseLower                 `json:"Mining Base Lower"`
	PlainsLake                      PlainsLake                      `json:"Plains Lake"`
	DetlasTrailEastPlains           DetlasTrailEastPlains           `json:"Detlas Trail East Plains"`
	LlevigarGateWest                LlevigarGateWest                `json:"Llevigar Gate West"`
	Cinfras                         Cinfras                         `json:"Cinfras"`
	LlevigarPlainsEastUpper         LlevigarPlainsEastUpper         `json:"Llevigar Plains East Upper"`
	SwampWestMid                    SwampWestMid                    `json:"Swamp West Mid"`
	SwampEastUpper                  SwampEastUpper                  `json:"Swamp East Upper"`
	SwampLower                      SwampLower                      `json:"Swamp Lower"`
	SwampPlainsBasin                SwampPlainsBasin                `json:"Swamp Plains Basin"`
	SwampMountainTransitionUpper    SwampMountainTransitionUpper    `json:"Swamp Mountain Transition Upper"`
	QuartzMinesNorthEast            QuartzMinesNorthEast            `json:"Quartz Mines North East"`
	MeteorCrater                    MeteorCrater                    `json:"Meteor Crater"`
	GoblinPlainsWest                GoblinPlainsWest                `json:"Goblin Plains West"`
	PreLightForestTransition        PreLightForestTransition        `json:"Pre-Light Forest Transition"`
	EfilimSouthPlains               EfilimSouthPlains               `json:"Efilim South Plains"`
	LightForestEntrance             LightForestEntrance             `json:"Light Forest Entrance"`
	LightForestNorthExit            LightForestNorthExit            `json:"Light Forest North Exit"`
	LightForestEastUpper            LightForestEastUpper            `json:"Light Forest East Upper"`
	MantisNest                      MantisNest                      `json:"Mantis Nest"`
	Gelibord                        Gelibord                        `json:"Gelibord"`
	FortressNorth                   FortressNorth                   `json:"Fortress North"`
	LexdalesPrison                  LexdalesPrison                  `json:"Lexdales Prison"`
	MesquisTower                    MesquisTower                    `json:"Mesquis Tower"`
	DarkForestVillage               DarkForestVillage               `json:"Dark Forest Village"`
	FungalGrove                     FungalGrove                     `json:"Fungal Grove"`
	MushroomHill                    MushroomHill                    `json:"Mushroom Hill"`
	AldoreiValleySouthEntrance      AldoreiValleySouthEntrance      `json:"Aldorei Valley South Entrance"`
	CinfrasCountyMidUpper           CinfrasCountyMidUpper           `json:"Cinfras County Mid-Upper"`
	GyliaLakeNorthWest              GyliaLakeNorthWest              `json:"Gylia Lake North West"`
	AldoreiValleyLower              AldoreiValleyLower              `json:"Aldorei Valley Lower"`
	AldoreiSWaterfall               AldoreiSWaterfall               `json:"Aldorei's Waterfall"`
	GhostlyPath                     GhostlyPath                     `json:"Ghostly Path"`
	Thanos                          Thanos                          `json:"Thanos"`
	MilitaryBaseUpper               MilitaryBaseUpper               `json:"Military Base Upper"`
	PathToOzothSSpireUpper          PathToOzothSSpireUpper          `json:"Path To Ozoth's Spire Upper"`
	CanyonPathNorthWest             CanyonPathNorthWest             `json:"Canyon Path North West"`
	CanyonLowerSouthEast            CanyonLowerSouthEast            `json:"Canyon Lower South East"`
	CanyonValleySouth               CanyonValleySouth               `json:"Canyon Valley South"`
	CanyonMountainEast              CanyonMountainEast              `json:"Canyon Mountain East"`
	CanyonSurvivor                  CanyonSurvivor                  `json:"Canyon Survivor"`
	WizardTowerNorth                WizardTowerNorth                `json:"Wizard Tower North"`
	ValleyOfTheLost                 ValleyOfTheLost                 `json:"Valley of the Lost"`
	CanyonHighPath                  CanyonHighPath                  `json:"Canyon High Path"`
	AirTempleUpper                  AirTempleUpper                  `json:"Air Temple Upper"`
	KandonFarm                      KandonFarm                      `json:"Kandon Farm"`
	EntranceToTheseadSouth          EntranceToTheseadSouth          `json:"Entrance to Thesead South"`
	TheseadSuburbs                  TheseadSuburbs                  `json:"Thesead Suburbs"`
	MoltenHeightsPortal             MoltenHeightsPortal             `json:"Molten Heights Portal"`
	ActiveVolcano                   ActiveVolcano                   `json:"Active Volcano"`
	SnailIsland                     SnailIsland                     `json:"Snail Island"`
	CorkusCity                      CorkusCity                      `json:"Corkus City"`
	RoadToMine                      RoadToMine                      `json:"Road To Mine"`
	CorkusForestSouth               CorkusForestSouth               `json:"Corkus Forest South"`
	CorkusDocks                     CorkusDocks                     `json:"Corkus Docks"`
	CorkusStatue                    CorkusStatue                    `json:"Corkus Statue"`
	FrozenFort                      FrozenFort                      `json:"Frozen Fort"`
	KandonRidge                     KandonRidge                     `json:"Kandon Ridge"`
	MoltenReach                     MoltenReach                     `json:"Molten Reach"`
	WybelIsland                     WybelIsland                     `json:"Wybel Island"`
	RaiderSBaseUpper                RaiderSBaseUpper                `json:"Raider's Base Upper"`
	SantaSHideout                   SantaSHideout                   `json:"Santa's Hideout"`
	AldoreiLowlands                 AldoreiLowlands                 `json:"Aldorei Lowlands"`
	RegularIsland                   RegularIsland                   `json:"Regular Island"`
	RoyalGate                       RoyalGate                       `json:"Royal Gate"`
	LostAtoll                       LostAtoll                       `json:"Lost Atoll"`
	TheSilentRoad                   TheSilentRoad                   `json:"The Silent Road"`
	ForgottenTown                   ForgottenTown                   `json:"Forgotten Town"`
	PathsOfSludge                   PathsOfSludge                   `json:"Paths of Sludge"`
	VoidValley                      VoidValley                      `json:"Void Valley"`
	HeavenlyIngress                 HeavenlyIngress                 `json:"Heavenly Ingress"`
	AzureFrontier                   AzureFrontier                   `json:"Azure Frontier"`
	NivlaWoodsEntrance              NivlaWoodsEntrance              `json:"Nivla Woods Entrance"`
	Maltic                          Maltic                          `json:"Maltic"`
	AbandonedFarm                   AbandonedFarm                   `json:"Abandoned Farm"`
	NorthNivlaWoods                 NorthNivlaWoods                 `json:"North Nivla Woods"`
	Detlas                          Detlas                          `json:"Detlas"`
	TwainLake                       TwainLake                       `json:"Twain Lake"`
	NesaakVillage                   NesaakVillage                   `json:"Nesaak Village"`
	JungleLake                      JungleLake                      `json:"Jungle Lake"`
	RymekWestUpper                  RymekWestUpper                  `json:"Rymek West Upper"`
	AlmujCity                       AlmujCity                       `json:"Almuj City"`
	Bremminglar                     Bremminglar                     `json:"Bremminglar"`
	NemractCathedral                NemractCathedral                `json:"Nemract Cathedral"`
	DurumIslesEast                  DurumIslesEast                  `json:"Durum Isles East"`
	DeadIslandNorthWest             DeadIslandNorthWest             `json:"Dead Island North West"`
	TernavesPlainsLower             TernavesPlainsLower             `json:"Ternaves Plains Lower"`
	DetlasSavannahTransition        DetlasSavannahTransition        `json:"Detlas Savannah Transition"`
	LlevigarFarmPlainsWest          LlevigarFarmPlainsWest          `json:"Llevigar Farm Plains West"`
	SwampEastLower                  SwampEastLower                  `json:"Swamp East Lower"`
	SwampDarkForestTransitionMid    SwampDarkForestTransitionMid    `json:"Swamp Dark Forest Transition Mid"`
	SwampMountainTransitionMid      SwampMountainTransitionMid      `json:"Swamp Mountain Transition Mid"`
	OrcLake                         OrcLake                         `json:"Orc Lake"`
	ForgottenPath                   ForgottenPath                   `json:"Forgotten Path"`
	EfilimSouthEastPlains           EfilimSouthEastPlains           `json:"Efilim South East Plains"`
	LightForestWestMid              LightForestWestMid              `json:"Light Forest West Mid"`
	PathToCinfras                   PathToCinfras                   `json:"Path to Cinfras"`
	MansionOfInsanity               MansionOfInsanity               `json:"Mansion of Insanity"`
	PathToTalor                     PathToTalor                     `json:"Path to Talor"`
	HeartOfDecay                    HeartOfDecay                    `json:"Heart of Decay"`
	CinfrasCountyLower              CinfrasCountyLower              `json:"Cinfras County Lower"`
	GertCamp                        GertCamp                        `json:"Gert Camp"`
	AldoreiSArch                    AldoreiSArch                    `json:"Aldorei's Arch"`
	PathToMilitaryBase              PathToMilitaryBase              `json:"Path To Military Base"`
	BanditCaveUpper                 BanditCaveUpper                 `json:"Bandit Cave Upper"`
	CanyonPathNorthMid              CanyonPathNorthMid              `json:"Canyon Path North Mid"`
	CanyonWaterfallMidNorth         CanyonWaterfallMidNorth         `json:"Canyon Waterfall Mid North"`
	MountainEdge                    MountainEdge                    `json:"Mountain Edge"`
	CliffsideValley                 CliffsideValley                 `json:"Cliffside Valley"`
	CliffsidePassageNorth           CliffsidePassageNorth           `json:"Cliffside Passage North"`
	EntranceToRodoroc               EntranceToRodoroc               `json:"Entrance to Rodoroc"`
	Ahmsord                         Ahmsord                         `json:"Ahmsord"`
	CorkusCitySouth                 CorkusCitySouth                 `json:"Corkus City South"`
	CorkusMountain                  CorkusMountain                  `json:"Corkus Mountain"`
	BloodyBeach                     BloodyBeach                     `json:"Bloody Beach"`
	DragonlingNests                 DragonlingNests                 `json:"Dragonling Nests"`
	SkyIslandAscent                 SkyIslandAscent                 `json:"Sky Island Ascent"`
	IcyDescent                      IcyDescent                      `json:"Icy Descent"`
	TwistedRidge                    TwistedRidge                    `json:"Twisted Ridge"`
	LighthousePlateau               LighthousePlateau               `json:"Lighthouse Plateau"`
	SinisterForest                  SinisterForest                  `json:"Sinister Forest"`
	BizarrePassage                  BizarrePassage                  `json:"Bizarre Passage"`
	PathToLight                     PathToLight                     `json:"Path to Light"`
	RagniEastSuburbs                RagniEastSuburbs                `json:"Ragni East Suburbs"`
	NemractQuarry                   NemractQuarry                   `json:"Nemract Quarry"`
	NivlaWoodsEdge                  NivlaWoodsEdge                  `json:"Nivla Woods Edge"`
	GreatBridgeJungle               GreatBridgeJungle               `json:"Great Bridge Jungle"`
	DesertUpper                     DesertUpper                     `json:"Desert Upper"`
	NemractRoad                     NemractRoad                     `json:"Nemract Road"`
	DujgonNation                    DujgonNation                    `json:"Dujgon Nation"`
	DesolateValley                  DesolateValley                  `json:"Desolate Valley"`
	LlevigarPlainsWestLower         LlevigarPlainsWestLower         `json:"Llevigar Plains West Lower"`
	Olux                            Olux                            `json:"Olux"`
	LoamsproutCamp                  LoamsproutCamp                  `json:"Loamsprout Camp"`
	AldoreiValleyWestEntrance       AldoreiValleyWestEntrance       `json:"Aldorei Valley West Entrance"`
	GelibordCastle                  GelibordCastle                  `json:"Gelibord Castle"`
	OldCrossroadsNorth              OldCrossroadsNorth              `json:"Old Crossroads North"`
	GyliaLakeSouthEast              GyliaLakeSouthEast              `json:"Gylia Lake South East"`
	BurningAirship                  BurningAirship                  `json:"Burning Airship"`
	CanyonWaterfallNorth            CanyonWaterfallNorth            `json:"Canyon Waterfall North"`
	ThanosExitUpper                 ThanosExitUpper                 `json:"Thanos Exit Upper"`
	CanyonOfTheLost                 CanyonOfTheLost                 `json:"Canyon Of The Lost"`
	LavaLakeBridge                  LavaLakeBridge                  `json:"Lava Lake Bridge"`
	LegendaryIsland                 LegendaryIsland                 `json:"Legendary Island"`
	PathToAhmsordLower              PathToAhmsordLower              `json:"Path to Ahmsord Lower"`
	JofashTunnel                    JofashTunnel                    `json:"Jofash Tunnel"`
	OrcBattlegrounds                OrcBattlegrounds                `json:"Orc Battlegrounds"`
	ToxicCaves                      ToxicCaves                      `json:"Toxic Caves"`
	PigmenRavines                   PigmenRavines                   `json:"Pigmen Ravines"`
	NesaakPlainsSouthWest           NesaakPlainsSouthWest           `json:"Nesaak Plains South West"`
	SavannahEastLower               SavannahEastLower               `json:"Savannah East Lower"`
	VolcanoLower                    VolcanoLower                    `json:"Volcano Lower"`
	SwampEastMidUpper               SwampEastMidUpper               `json:"Swamp East Mid-Upper"`
	RoadToLightForest               RoadToLightForest               `json:"Road To Light Forest"`
	EntranceToKander                EntranceToKander                `json:"Entrance to Kander"`
	AldoreiValleyUpper              AldoreiValleyUpper              `json:"Aldorei Valley Upper"`
	ThanosExit                      ThanosExit                      `json:"Thanos Exit"`
	CherryBlossomForest             CherryBlossomForest             `json:"Cherry Blossom Forest"`
	CorkusSeaPort                   CorkusSeaPort                   `json:"Corkus Sea Port"`
	Rodoroc                         Rodoroc                         `json:"Rodoroc"`
	FieldOfLife                     FieldOfLife                     `json:"Field of Life"`
	NetherGate                      NetherGate                      `json:"Nether Gate"`
	ZhightIsland                    ZhightIsland                    `json:"Zhight Island"`
	QuartzMinesSouthEast            QuartzMinesSouthEast            `json:"Quartz Mines South East"`
	DarkForestCinfrasTransition     DarkForestCinfrasTransition     `json:"Dark Forest Cinfras Transition"`
	KroltonSCave                    KroltonSCave                    `json:"Krolton's Cave"`
	SwampIsland                     SwampIsland                     `json:"Swamp Island"`
	RymekEastMid                    RymekEastMid                    `json:"Rymek East Mid"`
	LightForestEastLower            LightForestEastLower            `json:"Light Forest East Lower"`
	DernelJungleMid                 DernelJungleMid                 `json:"Dernel Jungle Mid"`
	LlevigarEntrance                LlevigarEntrance                `json:"Llevigar Entrance"`
	WormTunnel                      WormTunnel                      `json:"Worm Tunnel"`
	PathToOzothSSpireLower          PathToOzothSSpireLower          `json:"Path To Ozoth's Spire Lower"`
}
type Request struct {
	Timestamp int `json:"timestamp"`
	Version   int `json:"version"`
}
