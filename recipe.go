package gowynn

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

const cacheRecipeFile string = "recipe_list.json"

type Recipe struct {
}

// Get Returns a RecipeResult, which details public statistical information about the recipe.
// name is case sensitive and must be complete. You can find a list of all names by using GetList
func (r Recipe) Get(name string) (RecipeResult, error) {
	url := fmt.Sprintf(apiURL+"v2/recipe/get/%s", name)

	resp, err := doRequest(url)
	if err != nil {
		return RecipeResult{}, err
	}

	var result RecipeResult

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return RecipeResult{}, err
	}

	return result, nil
}

// UpdateList Returns a list of all recipe ids.
// It is recommended to call this route once, and cache the results upon each recipe update.
func (r Recipe) UpdateList() (RecipeList, error) {
	url := fmt.Sprintf(apiURL + "v2/recipe/list")

	resp, err := doRequest(url)
	if err != nil {
		return RecipeList{}, err
	}

	err = ioutil.WriteFile(cacheRecipeFile, resp, 0644)
	if err != nil {
		return RecipeList{}, err
	}

	var result RecipeList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return RecipeList{}, err
	}

	return result, nil
}

// GetList Returns a list of all recipe ids. It uses the cache file created by UpdateList
// If a cache file is not present this calls UpdateList and created the file
func (r Recipe) GetList() (RecipeList, error) {
	_, err := os.Stat(cacheRecipeFile)
	if os.IsNotExist(err) {
		result, err := r.UpdateList()
		if err != nil {
			return RecipeList{}, err
		}

		return result, nil
	}

	data, err := ioutil.ReadFile(cacheRecipeFile)

	var result RecipeList

	err = json.Unmarshal(data, &result)
	if err != nil {
		return RecipeList{}, err
	}

	return result, nil
}

// Refer to https://docs.wynncraft.com/Recipe-API/#search for documentation on how to use this endpoint
func (r Recipe) Search(query QueryType, args string) (RecipeResult, error) {
	url := fmt.Sprintf(apiURL+"v2/recipe/search/%s/%s", query, args)

	resp, err := doRequest(url)
	if err != nil {
		return RecipeResult{}, err
	}

	var result RecipeResult

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return RecipeResult{}, err
	}

	return result, nil
}
