# gowynn

gowynn is an API wrapper for Wynncraft's API written in Go

## Usage

This is an example on how to start using gowynn module
```go
package main

import (
	"fmt"
	"gitlab.com/SonoMichele/gowynn"
)

func main() {
	wynncraft := gowynn.NewWynncraft()
	
	guilds, _ := wynncraft.Guild.GetList()
	fmt.Println(guilds)
}
```

## Examples

You can find more examples on the [examples](examples) directory

## Contributing

Feel free to contribute by opening an [issue](https://gitlab.com/SonoMichele/gowynn/-/issues), a [merge request](https://gitlab.com/SonoMichele/gowynn/-/merge_requests) or by contacting me on [Telegram](https://t.me/sonomichelequellostrano) or [Mastodon](https://fosstodon.org/@SonoMichele)

## License

Refer to [LICENSE](LICENSE) file
