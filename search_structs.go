package gowynn

type SearchResult struct {
	Guilds  []string `json:"guilds"`
	Players []string `json:"players"`
	Search  string   `json:"search"`
	Request struct {
		Timestamp int `json:"timestamp"`
		Version   int `json:"version"`
	} `json:"request"`
}
