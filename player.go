package gowynn

import (
	"encoding/json"
	"fmt"
)

type Player struct {
}

// GetStats Returns a PlayerStats, which details public statistical information about the player.
// Note: Requests by UUID must be in dashed form (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)
func (p Player) GetStats(player string) (PlayerStats, error) {
	url := fmt.Sprintf(apiURL+"v2/player/%s/stats", player)

	resp, err := doRequest(url)
	if err != nil {
		return PlayerStats{}, err
	}

	var result PlayerStats

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return PlayerStats{}, err
	}

	return result, nil
}

// GetUUID Returns the uuid for a given username
// (this should not be used in place of the mojang api, and your IP will be blocked if you do so).
func (p Player) GetUUID(username string) (PlayerUUID, error) {
	url := fmt.Sprintf(apiURL+"v2/player/%s/uuid", username)

	resp, err := doRequest(url)
	if err != nil {
		return PlayerUUID{}, err
	}

	var result PlayerUUID

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return PlayerUUID{}, err
	}

	return result, nil
}
