package gowynn

import "time"

type GuildsLeaderboard struct {
	Request struct {
		Timestamp int     `json:"timestamp"`
		Version   float64 `json:"version"`
	} `json:"request"`
	Data []struct {
		Name    string    `json:"name"`
		Prefix  string    `json:"prefix"`
		Xp      int       `json:"xp"`
		Level   int       `json:"level"`
		Created time.Time `json:"created"`
		Banner  struct {
			Base   string `json:"base"`
			Tier   int    `json:"tier"`
			Layers []struct {
				Colour  string `json:"colour"`
				Pattern string `json:"pattern"`
			} `json:"layers"`
		} `json:"banner,omitempty"`
		Territories  int `json:"territories"`
		MembersCount int `json:"membersCount"`
		Num          int `json:"num"`
		Bank         struct {
			PubBank []struct {
				Item string `json:"item"`
				Nbt  string `json:"nbt"`
			} `json:"pubBank"`
		} `json:"bank,omitempty"`
		PubBank int `json:"pubBank,omitempty"`
	} `json:"data"`
}

type PlayerLeaderboard struct {
	Request struct {
		Timestamp int     `json:"timestamp"`
		Version   float64 `json:"version"`
	} `json:"request"`
	Data []struct {
		Name       string `json:"name"`
		UUID       string `json:"uuid"`
		Kills      int    `json:"kills"`
		Level      int    `json:"level"`
		Xp         int64  `json:"xp"`
		MinPlayed  int    `json:"minPlayed"`
		Tag        string `json:"tag"`
		Rank       string `json:"rank"`
		DisplayTag bool   `json:"displayTag"`
		Veteran    bool   `json:"veteran"`
		GuildTag   string `json:"guildTag,omitempty"`
		Guild      string `json:"guild,omitempty"`
		Num        int    `json:"num"`
	} `json:"data"`
}

type PvPLeaderboard struct {
	Request struct {
		Timestamp int     `json:"timestamp"`
		Version   float64 `json:"version"`
	} `json:"request"`
	Data []struct {
		Name       string `json:"name"`
		UUID       string `json:"uuid"`
		Kills      int    `json:"kills"`
		Level      int    `json:"level"`
		Xp         int    `json:"xp"`
		MinPlayed  int    `json:"minPlayed"`
		Tag        string `json:"tag"`
		Rank       string `json:"rank"`
		DisplayTag bool   `json:"displayTag"`
		Veteran    bool   `json:"veteran"`
		GuildTag   string `json:"guildTag,omitempty"`
		Guild      string `json:"guild,omitempty"`
		Num        int    `json:"num"`
	} `json:"data"`
}
