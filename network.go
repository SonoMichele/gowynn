package gowynn

import (
	"encoding/json"
	"fmt"
)

type Network struct {
}

// GetServerList Returns which servers are online and which players are on them.
func (n Network) GetServerList() (ServerList, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=onlinePlayers")

	resp, err := doRequest(url)
	if err != nil {
		return ServerList{}, err
	}

	var result ServerList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return ServerList{}, err
	}

	return result, nil
}

// GetPlayerSum Returns the player sum on the wynncraft network.
func (n Network) GetPlayerSum() (PlayerSum, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=onlinePlayersSum")

	resp, err := doRequest(url)
	if err != nil {
		return PlayerSum{}, err
	}

	var result PlayerSum

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return PlayerSum{}, err
	}

	return result, nil
}
