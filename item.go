package gowynn

import (
	"encoding/json"
	"errors"
	"fmt"
)

type ItemCategory string

const (
	ALL        ItemCategory = "all"
	HELMET     ItemCategory = "helmet"
	CHESTPLATE ItemCategory = "chestplate"
	LEGGINGS   ItemCategory = "leggings"
	BOOTS      ItemCategory = "boots"
	RING       ItemCategory = "ring"
	NECKLACE   ItemCategory = "necklace"
	BRACELET   ItemCategory = "bracelet"
	DAGGER     ItemCategory = "dagger"
	SPEAR      ItemCategory = "spear"
	BOW        ItemCategory = "bow"
	WAND       ItemCategory = "wand"
)

func (ic ItemCategory) isValid() error {
	switch ic {
	case ALL, HELMET, CHESTPLATE, LEGGINGS, BOOTS, RING, NECKLACE,
		BRACELET, DAGGER, SPEAR, BOW, WAND:
		return nil
	}

	return errors.New("Invalid ItemCategory type")
}

type Item struct {
}

// SearchByCategory Returns a list of wynncraft items matching the search category ItemCategory.
func (i Item) SearchByCategory(category ItemCategory) (ItemsList, error) {
	err := category.isValid()
	if err != nil {
		return ItemsList{}, err
	}

	url := fmt.Sprintf(apiURL+"public_api.php?action=itemDB&category=%s", category)

	resp, err := doRequest(url)
	if err != nil {
		return ItemsList{}, err
	}

	var result ItemsList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return ItemsList{}, err
	}

	return result, nil
}

// SearchByName Returns a list of wynncraft items matching the search name.
// The name is case insensitive and can be partial.
func (i Item) SearchByName(name string) (ItemsList, error) {
	url := fmt.Sprintf(apiURL+"public_api.php?action=itemDB&search=%s", name)

	resp, err := doRequest(url)
	if err != nil {
		return ItemsList{}, err
	}

	var result ItemsList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return ItemsList{}, err
	}

	return result, nil
}
