package gowynn

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

const cacheIngredientFile = "ingredient_list.json"

type Ingredient struct {
}

// Get Returns a IngredientResult, which details public statistical information about the ingredient.
// Note: this function replaces spaces with _ so you won't get error 502 as stated at https://docs.wynncraft.com/Ingredient-API/#get
func (i Ingredient) Get(name string) (IngredientResult, error) {
	name = strings.ReplaceAll(name, " ", "_")
	url := fmt.Sprintf(apiURL+"v2/ingredient/get/%s", name)

	resp, err := doRequest(url)
	if err != nil {
		return IngredientResult{}, err
	}

	var result IngredientResult

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return IngredientResult{}, err
	}

	return result, nil
}

// UpdateList Returns a list of all recipe ids.
// It is recommended to call this route once, and cache the results upon each recipe update.
func (i Ingredient) UpdateList() (IngredientList, error) {
	url := fmt.Sprintf(apiURL + "v2/ingredient/list")

	resp, err := doRequest(url)
	if err != nil {
		return IngredientList{}, err
	}

	err = ioutil.WriteFile(cacheIngredientFile, resp, 0644)
	if err != nil {
		return IngredientList{}, err
	}

	var result IngredientList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return IngredientList{}, err
	}

	return result, nil
}

// GetList Returns a list of all ingredients ids. It uses the cache file created by UpdateList
// If a cache file is not present this calls UpdateList and created the file
func (i Ingredient) GetList() (IngredientList, error) {
	_, err := os.Stat(cacheIngredientFile)
	if os.IsNotExist(err) {
		result, err := i.UpdateList()
		if err != nil {
			return IngredientList{}, err
		}

		return result, nil
	}

	data, err := ioutil.ReadFile(cacheIngredientFile)

	var result IngredientList

	err = json.Unmarshal(data, &result)
	if err != nil {
		return IngredientList{}, err
	}

	return result, nil
}

// Refer to https://docs.wynncraft.com/Ingredient-API/#search for documentation on how to use this endpoint
func (i Ingredient) Search(query QueryType, args string) (IngredientResult, error) {
	url := fmt.Sprintf(apiURL+"v2/ingredient/search/%s/%s", query, args)

	resp, err := doRequest(url)
	if err != nil {
		return IngredientResult{}, err
	}

	var result IngredientResult

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return IngredientResult{}, err
	}

	return result, nil
}
