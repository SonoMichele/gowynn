package gowynn

import (
	"encoding/json"
	"fmt"
)

type Territory struct {
}

// GetList Returns a struct containing all wynncraft's territories and their status.
func (t Territory) GetList() (TerritoryList, error) {
	url := fmt.Sprintf(apiURL + "public_api.php?action=territoryList")

	resp, err := doRequest(url)
	if err != nil {
		return TerritoryList{}, err
	}

	var result TerritoryList

	err = json.Unmarshal(resp, &result)
	if err != nil {
		return TerritoryList{}, err
	}

	return result, nil
}
