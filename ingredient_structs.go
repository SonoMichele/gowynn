package gowynn

type IngredientResult struct {
	Kind      string `json:"kind"`
	Code      int    `json:"code"`
	Timestamp int64  `json:"timestamp"`
	Version   string `json:"version"`
	Data      []struct {
		Name   string   `json:"name"`
		Tier   int      `json:"tier"`
		Level  int      `json:"level"`
		Skills []string `json:"skills"`
		Sprite struct {
			ID     int `json:"id"`
			Damage int `json:"damage"`
		} `json:"sprite"`
		Identifications struct {
			AIRDEFENSE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"AIRDEFENSE,omitempty"`
			EARTHDEFENSE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"EARTHDEFENSE,omitempty"`
			FIREDEFENSE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"FIREDEFENSE,omitempty"`
			THUNDERDEFENSE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"THUNDERDEFENSE,omitempty"`
			WATERDEFENSE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"WATERDEFENSE,omitempty"`
			DAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"DAMAGEBONUS,omitempty"`
			DAMAGEBONUSRAW struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"DAMAGEBONUSRAW,omitempty"`
			AIRDAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"AIRDAMAGEBONUS,omitempty"`
			EARTHDAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"EARTHDAMAGEBONUS,omitempty"`
			FIREDAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"FIREDAMAGEBONUS,omitempty"`
			THUNDERDAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"THUNDERDAMAGEBONUS,omitempty"`
			WATERDAMAGEBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"WATERDAMAGEBONUS,omitempty"`
			AGILITYPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"AGILITYPOINTS,omitempty"`
			DEFENSEPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"DEFENSEPOINTS,omitempty"`
			DEXTERITYPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"DEXTERITYPOINTS,omitempty"`
			INTELLIGENCEPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"INTELLIGENCEPOINTS,omitempty"`
			STRENGTHPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"STRENGTHPOINTS,omitempty"`
			POISON struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"POISON,omitempty"`
			MANASTEAL struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"MANASTEAL,omitempty"`
			MANAREGEN struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"MANAREGEN,omitempty"`
			SPEED struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"SPEED,omitempty"`
			HEALTHBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"HEALTHBONUS,omitempty"`
			SPELLDAMAGE struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"SPELLDAMAGE,omitempty"`
			SPELLDAMAGERAW struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"SPELLDAMAGERAW,omitempty"`
			ATTACKSPEED struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"ATTACKSPEED,omitempty"`
			LIFESTEAL struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"LIFESTEAL,omitempty"`
			HEALTHREGEN struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"HEALTHREGEN,omitempty"`
			HEALTHREGENRAW struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"HEALTHREGENRAW,omitempty"`
			REFLECTION struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"REFLECTION,omitempty"`
			THORNS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"THORNS,omitempty"`
			EXPLODING struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"EXPLODING,omitempty"`
			LOOTBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"LOOTBONUS,omitempty"`
			XPBONUS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"XPBONUS,omitempty"`
			EMERALDSTEALING struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"EMERALDSTEALING,omitempty"`
			SOULPOINTS struct {
				Minimum int `json:"minimum,omitempty"`
				Maximum int `json:"maximum,omitempty"`
			} `json:"SOULPOINTS,omitempty"`
		} `json:"identifications"`
		ItemOnlyIDs struct {
			DurabilityModifier      int `json:"durabilityModifier"`
			StrengthRequirement     int `json:"strengthRequirement"`
			DexterityRequirement    int `json:"dexterityRequirement"`
			IntelligenceRequirement int `json:"intelligenceRequirement"`
			DefenceRequirement      int `json:"defenceRequirement"`
			AgilityRequirement      int `json:"agilityRequirement"`
		} `json:"itemOnlyIDs"`
		ConsumableOnlyIDs struct {
			Duration int `json:"duration"`
			Charges  int `json:"charges"`
		} `json:"consumableOnlyIDs"`
		IngredientPositionModifiers struct {
			Left        int `json:"left"`
			Right       int `json:"right"`
			Above       int `json:"above"`
			Under       int `json:"under"`
			Touching    int `json:"touching"`
			NotTouching int `json:"notTouching"`
		} `json:"ingredientPositionModifiers"`
	} `json:"data"`
}

type IngredientList struct {
	Kind      string   `json:"kind"`
	Code      int      `json:"code"`
	Timestamp int64    `json:"timestamp"`
	Version   string   `json:"version"`
	Data      []string `json:"data"`
}
